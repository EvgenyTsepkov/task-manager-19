package ru.tsc.tsepkov.tm.command.user;

import ru.tsc.tsepkov.tm.util.TerminalUtil;

public class UserRemoveByLogin extends AbstractUserCommand {

    public static final String NAME = "remove-user-by-login";

    public static final String DESCRIPTION = "Remove user by login.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
    }

}
