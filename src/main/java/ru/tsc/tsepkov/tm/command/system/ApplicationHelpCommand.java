package ru.tsc.tsepkov.tm.command.system;

import ru.tsc.tsepkov.tm.api.ICommand;
import ru.tsc.tsepkov.tm.command.AbstractCommand;

import java.util.Collection;

public class ApplicationHelpCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-h";

    public static final String NAME = "help";

    public static final String DESCRIPTION = "Show list arguments.";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command: commands) System.out.println(command.toString());
    }

}
