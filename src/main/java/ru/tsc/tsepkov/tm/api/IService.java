package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
