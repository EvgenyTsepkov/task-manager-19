package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.Task;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

}
