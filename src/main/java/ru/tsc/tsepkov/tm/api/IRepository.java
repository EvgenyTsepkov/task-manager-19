package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.enumerated.Sort;
import ru.tsc.tsepkov.tm.model.AbstractModel;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    void remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    boolean existsById(String id);

    void clear();

    int getSize();

}
