package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>{

}
