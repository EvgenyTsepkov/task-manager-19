package ru.tsc.tsepkov.tm.repository;

import ru.tsc.tsepkov.tm.api.ITaskRepository;
import ru.tsc.tsepkov.tm.model.Task;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : records) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

}
