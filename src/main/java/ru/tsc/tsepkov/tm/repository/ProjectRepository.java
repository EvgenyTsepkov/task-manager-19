package ru.tsc.tsepkov.tm.repository;

import ru.tsc.tsepkov.tm.api.IProjectRepository;
import ru.tsc.tsepkov.tm.model.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

}
