package ru.tsc.tsepkov.tm.service;

import ru.tsc.tsepkov.tm.api.IRepository;
import ru.tsc.tsepkov.tm.api.IService;
import ru.tsc.tsepkov.tm.enumerated.Sort;
import ru.tsc.tsepkov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.tsepkov.tm.exception.field.IdEmptyException;
import ru.tsc.tsepkov.tm.exception.field.IndexIncorrectException;
import ru.tsc.tsepkov.tm.model.AbstractModel;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort);
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public void remove(final M model) {
        if (model == null) throw new ModelNotFoundException();
        repository.remove(model);
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

}
