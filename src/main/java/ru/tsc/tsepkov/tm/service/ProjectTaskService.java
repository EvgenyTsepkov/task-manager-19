package ru.tsc.tsepkov.tm.service;

import ru.tsc.tsepkov.tm.api.IProjectRepository;
import ru.tsc.tsepkov.tm.api.IProjectService;
import ru.tsc.tsepkov.tm.api.IProjectTaskService;
import ru.tsc.tsepkov.tm.api.ITaskRepository;
import ru.tsc.tsepkov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tsepkov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.tsepkov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.tsepkov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.tsepkov.tm.model.Task;
import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    private final IProjectService projectService;

    public ProjectTaskService(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository,
            final IProjectService projectService
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.projectService = projectService;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(projectId)) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

}
